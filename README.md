# Governance Analytics Knowledge Base

************************************************

# Structure of the knowledge base
## [1. Introduction](#Introduction)

## [2. Main Tools](#Maintools)

## [3. Databases](#Databases)

## [4. Similar Project](#Similar-Projects)

*******************************************************************

# <a name="Introduction"></a>Introduction

## Purpose of the knowledge base

You are here in the knowledge base section of the Governance Analytics website. The purpose of this knowledge base is to provide the researchers with the necessary approaches, tools, and databases access to deal with data intensive research.

## The stakes of dealing with complex data

The new stakes of data science have been largely covered in the media, for instance in [*The Guardian*](https://t.co/redirect?url=https%3A%2F%2Ft.co%2FyaFLyVMNiw%3Fcn%3DZmF2b3JpdGU%253D&t=1&cn=ZmF2b3JpdGU%3D&sig=00bd3675233992697b18416eb086e4a289175c34&iid=e9ab64b5e67a4772bdd1af48c187e523&uid=341919589&nid=5+262) 

## Short presentation of the content of the knowledge base

This knowledge base aims at answering a set of simple questions the researcher asks himself when dealing with data intensive research. It is intended for beginners as well as for experts. For each part, the Governance Analytics team provides you with basics, as well as with more elaborated aspects. 

We begin first by presenting the [Main tools](#Maintools) which can be used by researchers. We then present the available Databases accessible to researchers. 

The Main tools allow to extract and process data. There is first a [Data extraction and cleaning phase](#first). Data extraction from the web has often to be followed by a Data cleaning procedure. The procedure then depends from the nature of the data, textual or numerical. 

Textual data can be the object of a Textual analysis. The researcher can turn to the Use of semantic technologies to annotate and search data. Numerical data must be the object of a Classification and analysis. After a phase of Classification, the researcher may wish to perform Clustering, Forecasting, Regression Analysis, and Knowledge discovery.

Numerical data, or textual data converted into numerical information, can then be the subject of an [Econometric analysis](#third). We here provide for the use of the beginner researcher in those field a short Introduction to econometrics, before providing an exhaustive set of Resources for econometrics for more advanced users.

The second main purpose of this knowledge base is to provide the beginner or advanced researcher with a set of Databases. We do not aim at exhaustivity, and are waiting for your input to expand this knowledge base.

The recent evolution of data science has been marked by the expansion of Open data or openly accessible data. This can stem whether from Government data, or from NGO data. These databases can also be linked through Linked open data and linked repositories.

We present more [traditional datasets](#Databases) from a wide range of data providers. The traditional datasets have recently been challenged by the rise of long run Historical datasets. Such kind of data often rest on the exploitation of physical archives, of which we present here the most interesting examples and possibilities.

We finally conclude by presenting comparable Knowledge bases in the world, which may be of particular help for the researcher to expand his quest for information on data-driven research.

This knowledge base aims at being a Work in progress. The Governance Analytics team is waiting for your input in order to make it even more user friendly, and to incorporate any data tools or databases you think would be of particular interest for this project. Please don’t hesitate to write to the Governance Analytics project's coordinator, Bruno Chaves (bruno.chaves@dauphine.fr) for ideas and suggestions.

************************************************************************

##  <a name="Maintools"></a>Main tools 
    
### <a name="first"> 1. [Data extraction and cleaning](DataExtractionCleaning.md)</a> 
 * Data extraction from the web
 * Data cleaning
 * Textual analysis
 * Use of semantic technologies to annotate and search data
 * Methodologies of database building
    * Version controlling
    * Data consistency checking

### <a name="second"> 2.[Data classification and analysis](dataclasssificationanalysis.md)  </a> 
* Classification methods
* Clustering algorithms
* Forecasting
    * Regression analysis
    * Knowledge discovery
    
### <a name="third"> 3. Econometric analysis for social sciences</a>
* [A general introduction to Econometrics](introeconometrics.md)
* [Important references to common problems](importantreferences.md)
* [FAQs](faqeconometrics.md)
* [Resources for econometric analyses](resourceseconometrics.md)


##  <a name="Databases"></a>Databases 


### 1. [Open data and openly accessible data](OpenDataOpenAccessData.md)
* Government data
* NGO data
* Linked open data and linked open vocabularies 

### 2. [Online Datasets and Archives (sorted by disciplines and topics) ](Databases.md)


## <a name="Similar-Projects"> </a>Similar Projects 

Here is a list of potentially interesting [Similar Projects](SimilarInitiatives.md)  



**************************************************************************************

# Participate to our information base

The Governance Analytics team is waiting for your input in order to make it even more user friendly, and to incorporate any data tools or databases you think would be of particular interest for this project. Please don’t hesitate to write to us for ideas and suggestions.


.