# Databases

Data is the backbone of any empirical research. Governments, companies, individuals are dependent on data for decision-making. Numerous institutions have compiled useful data for the public to use.

One particular field of expansion was long run data drawn from a variety of sources. They allow the researcher to expand his research back in time. The number of such databases has been expanding rapidly. We provide first here the Main prominent historical databases to be explored on a priority basis, before dwelling in details on a list of existing Detailed databases about specific domains. For each databases, we provide a link to the presentation of the database, and a direct link to the data, if available. Unless stated otherwise, all those databases are free of charge.

The Governance Analytics Team has gathered certain of those data series in integrated Excel files. You are welcome to ask Julien Brault (julien.brault@dauphine.fr) if we already have gathered the data you are looking for.



## [General](databasegeneral.md)
## [Economics](databaseseconomics.md)
## [Finance](databasefinance.md)
## [Political Science](databasepolitical.md)
## [Cultural Studies and Sociology](databaseculture.md)
## [Demography and Geography](databasedemography.md)

