# Sources of Interesting Data for Social Sciences
They are all free-access data sources. Some of them may require registration with contact information.

## Data Collections

### Data Collection: World DataBank
- Discipline: Economics
- Author: World Bank
- Link: http://databank.worldbank.org/data/home.aspx
- Description: Numerous types of economic data can be found in the World DataBank.

### Data Collection: Eurostat
- Discipline: Economics, Sociology
- Author: European Commission
- Link: http://ec.europa.eu/eurostat
- Description: It contains data on various economic and social topics.

### Data Collection: OECD Data
- Discipline: Economics, Sociology
- Author: OECD
- Link: https://data.oecd.org
- Description: It contains data on agriculture, development, economy, education, energy, environment, finance, government, health, innovation and technology, jobs, and society.

### Data Collection: Google Public Data
 - Discipline: Any
 - Author: Google
 - Link: (https://www.google.com/publicdata/directory?hl=en_US&dl=en_US#!)
 - Description: It is a platform for different data providers to publish their information and facilitate distribution of knowledge.

### Data Collection: Journal of Applied Econometrics
- Discipline: Economics
- Author: Journal of Applied Econometrics
- Link: http://qed.econ.queensu.ca/jae/
- Description: It contains all datasets used by the papers published in the JAE since 1988.

## Data by Google 
### Google n-gram Viewer
 - Discipline: Any
 - Author: Google
 - Coverage: 1800-2000
 - Link: https://books.google.com/ngrams/
 - Description: Researchers can trace the numbers of occurrance of terms in books over years.

### Google Trends
- Discipline: Any
- Author: Google
- Coverage: 2004 - Now
- Link: https://www.google.com/trends/
- Description: Researchers can input terms or topics in Google Trends and find the search volumes of them over time. Comparison among terms is possible. Despite some limitations, one can also check the regional data.

##  Cultural Data
### World Value Survey
- Discipline: Any
- Author: World Value Survey
- Coverage: 1981 - 2014
- Link: http://www.worldvaluessurvey.org/wvs.jsp
- Description: The survey collects responses of subjects on various issues, including political view, happiness, religious view, saving and borrowing preference, and so on.
- Remark: Free of charge. It requires filling a simple form with user's name, institution and the purpose of the use of data.

### Religiosity Index
- Discipline: Sociology, Religion Studies
- Author: Diener, Ed, Louis Tay and David G. Myers
- Coverage: 2011, World and US States
- Link: http://psycnet.apa.org/journals/psp/101/6/1278/
- Description: They used the Gallup Poll to measure religiosity of each state of US.

### 6D Model of National Culture
- Discipline: Economics, Sociology
- Author: Geert Hofstede
- Link: http://geerthofstede.com/research-and-vsm/dimension-data-matrix/
- Description: The six dimensions include "Individualism", "Power Distance", "Masculinity", "Uncertainty Avoidance", Long-term Orientation", and "Indulgence".

### Measure of Racial Animus in US
- Discipline: Economics, Sociology
- Coverage: 2008, US States
- Author: Seth Stephens-Davidowitz
- Link: http://www.sciencedirect.com/science/article/pii/S0047272714000929
- Description: It ranks US States in terms of racial animus.

## Political Science
### Cohesion Party Score
- Discipline: Political Science
- Author: Jahn, Detlet and Christoph Oberst
- Coverage: 1950-2005, OECD
- Link: http://comparativepolitics.uni-greifswald.de/data.html
- Citation: Jahn, Detlef and Christoph Oberst. 2012. “PIP – Parties, Institutions & Preferences: Left-Right Party Cohesion. [Replication Data]. Chair of Comparative Politics, University of Greifswald. 

### Corporatism
- Discipline: Political Science
- Author: Jahn, Detlef
- Coverage: 1960-2010, 42 Countries
- Link: http://comparativepolitics.uni-greifswald.de/data.html
- Citation: Jahn, Detlef (2014). "Changing the Guard: Trends in Corporatist Arrangements in 42 Highly Industrialized Societies from 1960 to 2010." Socio-Economic Review, online first: August 26, 2014. doi: 10.1093/ser/mwu028

### Democratic Electoral Systems
- Discipline: Political Science
- Author: Bormann, Nils-Christian and Matt Golder
- Coverage: 1946-2011
- Link: http://mattgolder.com/elections
- Citation: Nils-Christian Bormann & Matt Golder. 2013. “Democratic electoral Systems Around the World, 1946-2011.” Electoral Studies 32: 360-369.

### Left-Right Party Score
- Discipline: Political Science
- Author: Jahn, Detlet, Thomas Behm, Nils Düpon, and Christoph Oberst
- Coverage: 1944-2013, 23 OECD
- Link: http://comparativepolitics.uni-greifswald.de/data.html
- Citation: Jahn, Detlef, Thomas Behm, Nils Düpont, and Christoph Oberst. 2014. “PIP – Parties, Institutions & Preferences: Left-Right Party Scores. [Version 2014-09].” Chair of Comparative Politics, University of Greifswald. 

### Legal Origins
- Discipline: Political Science
- Author: LaPorta, Rafael, Florencio Lopez-de-Silanes, and Andrei Shleifer
- Link: http://scholar.harvard.edu/shleifer/publications/economic-consequences-legal-origins
- Citation: LaPorta, Rafael, Florencio Lopez-de-Silanes, and Andrei Shleifer. 2008. “The Economic Consequences of Legal Origins.” Journal of Economic Literature 46 (2): 285-332.

### Manifesto Project
- Discipline: Political Science
- Author: Manifesto Project 
- Coverage: 1945-2015, Parties of Most Countries
- Link: https://manifestoproject.wzb.eu/datasets
- Citation: Volkens, Andrea / Lehmann, Pola / Matthieß, Theres / Merz, Nicolas / Regel, Sven with Werner, Annika (2016): The Manifesto Data Collection. Manifesto Project (MRG/CMP/MARPOR). Version 2016a. Berlin: Wissenschaftszentrum Berlin für Sozialforschung (WZB)
- Remark: Information is available upon registration.

### Political Rights, Civil Liberties
- Discipline: Political Science
- Author: Freedom House
- Coverage: 1973-2016
- Link: https://freedomhouse.org/report-types/freedom-world

### Polity IV Annual Time-Series
- Discipline: Political Science
- Author: Monty G. Marshall and Ted Robert Gurr
- Coverage: 1800-2015
- Link: http://www.systemicpeace.org/inscrdata.html
- Description: It is a commonly used measure for autocracy/democracy (from -7 to +7).

### Veto Players
- Discipline: Political Science
- Author: Jahn, Detlet, Thomas Behm, Nils Düpon, and Christoph Oberst
- Coverage: 1944-2012, 23 OECD Countries
- Link: http://comparativepolitics.uni-greifswald.de/data.html
- Citation: Jahn, Detlef, Thomas Behm, Nils Düpont, and Christoph Oberst. 2014. “PIP – Parties, Institutions & Preferences: Veto Player (Annual) [Version 2014-09].” Chair of Comparative Politics, University of Greifswald. 

### Eurobarometer
- Discipline: Political Science
- Author: European Commision
- Coverage: 1974-2016, EU
- Link: http://ec.europa.eu/COMMFrontOffice/publicopinion/index.cfm/Survey/index#p=1&yearFrom=1974&yearTo=1975
- Description: It involves standard and special Eurobarometer that covers a broad range of topics concerning the EU and its policies.

## Economic Data
### Digital Agenda Scoreboard
- Discipline: Economics
- Author: European Commission
- Coverage: 2000-2015, EU
- Link: http://digital-agenda-data.eu/datasets/digital_agenda_scoreboard_key_indicators/
- Description: It contains different measurements of the digital economy in the following five dimensions: Connectivity, Human Capital, Use of Internet, Integration of Digital Technology, and Digital Public Services.

### International Migration
- Discipline: Economics, Political Science, Sociology
- Author: United Nations
- Coverage: 1990, 2000, 2010, 2013
- Link: http://www.un.org/en/development/desa/population/migration/data/estimates2/estimatesorigin.shtml
- Description: It provides the information of the migrants' origins and destinations.

### Crime data
- Discipline: Economics, Sociology
- Author: European Commision
- Coverage: 2008-2014, EU
- Link: http://ec.europa.eu/eurostat/web/crime/database

### Happy Planet Index
- Discipline: Economics, Sociology
- Author: New Economics Foundation
- Link: http://happyplanetindex.org/about

### Human Development Index
- Discipline: Economics, Sociology
- Author: UN Development Programme
- Coverage: 1990-2014
- Link: http://hdr.undp.org/en/data-explorer
- Description: The Human Development Index (HDI) is a summary measure of average achievement in key dimensions of human development: a long and healthy life, being knowledgeable and have a decent standard of living. The HDI is the geometric mean of normalized indices for each of the three dimensions.

## Others
### Geography Data
- Discipline: Geography
- Author: Expedia
- Link: http://developer.ean.com/database/geography-data/
- Description: It provides cooridnates of cities, points of interest.

### Incidence and Mortality data of 24 types of Cancer
- Discipline: Health Economics
- Author: Ferlay J, Steliarova-Foucher E, Lortet-Tieulent J, Rosso S, Coebergh JWW, Comber H, Forman D, Bray F.
- Link: http://eu-cancer.iarc.fr/eucan/Default.aspx
- Citation: Ferlay J, Steliarova-Foucher E, Lortet-Tieulent J, Rosso S, Coebergh JWW, Comber H, Forman D, Bray F. Cancer incidence and mortality patterns in Europe: estimates for 40 countries in 2012. Eur J Cancer. 2013 Apr;49(6):1374-403. doi: 10.1016/j.ejca.2012.12.027; Bray F, Ren JS, Masuyer E, Ferlay J. Estimates of global cancer prevalence for 27 sites in the adult population in 2008. Int J Cancer. 2013 Mar 1;132(5):1133-45. doi: 10.1002/ijc.27711. Epub 2012 Jul 26.